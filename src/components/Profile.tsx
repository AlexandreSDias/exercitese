import { useContext } from 'react';
import { ChallengesContext } from '../contexts/ChallengesContext';
import styles from '../styles/components/Profile.module.css';

export function Profile(){
    const {level} = useContext(ChallengesContext);
    return(
        <div className={styles.profileContainer}>
            <img src="https://gitlab.com/uploads/-/system/user/avatar/8175287/avatar.png?width=400" alt="Alexandre Dias"/>
            <div>
                <strong>Alexandre Dias</strong>
                <p>
                    <img src="icons/level.svg" alt="Level icon"/>
                    Level: {level}
                    </p>
            </div>
        </div>
    );
} 